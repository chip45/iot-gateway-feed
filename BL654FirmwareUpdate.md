# Firmware Update of the BL654 Bluetooth Module
The up2net IoT-Gateway uses Laird's BL654 Bluetooth 5 LE module for bluetooth connectivity. This document describes how to upgrade the BL654 firmware to the latest version if necessary.
## Prerequisites
Get latest BL654 Firmware fom https://www.lairdconnect.com/wireless-modules/bluetooth-modules/bluetooth-5-modules/bl654-series

Get virtual COM-Port driver from https://github.com/Raggles/com0com/releases
## Configure up2net IoT-Gateway
### Configure package ser2net with vi
The up2net IoT-Gateway comes with a suitable configuration, so you normally can skip this section. If you think you need to change things, you can edit the config file:
```
vi /etc/config/ser2net
```
This is the default configuration for reference.
```
config ser2net global
        option enabled 1
<code>
config controlport
        option enabled 0
        option host localhost
        option port 2000

config default
        option speed 115200
        option databits 8
        option parity 'none'
        option stopbits 1
        option rtscts false
        option local false
        option remctl true

config proxy
        option enabled 1
        option port 8000
        option protocol raw
        option timeout 30
        option device '/dev/ttyS1'
        option baudrate 115200
        option databits 8
        option parity 'none'
        option stopbits 1
        option rtscts false
        option local false
        option xonxoff false
        list options ''
```
### Restart ser2net Service
Restart the ser2net service to apply the changes:
```
/etc/init.d/ser2net restart
```
## Install Virtual COM-port Driver on the PC
- Unzip package
- Start setupg.exe
- Press "Add Pair"
- Select "Virtual Port Pair 0"
- Change "CNCB0" to "COMxx" (choose a free COM port on your PC)
- Press "Apply"
## Start com2tcp.exe
- Open a command shell (cmd.exe)
- Issue the following command with the appropriate IP address:
```
com2tcp.exe --baud 115200 \\.\CNCA0 192.168.1.1 8000
```
Now your above selected COM port should be connected to the UART of the BL654 module.
## Start Firmware Update from your PC
The firmware image as well as the update tool is included in the above download firmware ZIP archive. You can now start the update tool:
- Start BL654xUartFwUpgrade.exe
- Press "OK"
- Select "COMxx"
- Press "Proceed"

Now the regular firmware upgrade process should run. Close the update tool at the end.
## Check New Firmware Version
Go to the IoT-Gateway console and connect with microcom to the BL654.
```
microcom -s 115200 /dev/ttyS1
```
Enter the following command and press enter:
```
at i 3
```
The version of the current firmware will be displayed.
