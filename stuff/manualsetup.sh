#!/bin/sh
# up2net manual setup gpio
#opkg install luci-proto-3g luci-proto-ncm block-mount fdisk kmod-sdhci-mt7620 kmod-fs-ext4 kmod-fs-vfat kmod-nls-cp437 kmod-nls-iso8859-1 kmod-nls-utf8 kmod-usb-net-qmi-wwan kmod-usb-serial-option
echo 11 >/sys/class/gpio/export
echo 15 >/sys/class/gpio/export
echo 16 >/sys/class/gpio/export
echo 17 >/sys/class/gpio/export
echo 18 >/sys/class/gpio/export
echo 19 >/sys/class/gpio/export
echo 2 >/sys/class/gpio/export
echo 3 >/sys/class/gpio/export
echo 14 >/sys/class/gpio/export
echo 5 >/sys/class/gpio/export
echo out >/sys/class/gpio/gpio11/direction
echo out >/sys/class/gpio/gpio15/direction
echo out >/sys/class/gpio/gpio16/direction
echo out >/sys/class/gpio/gpio17/direction
echo out >/sys/class/gpio/gpio18/direction
echo out >/sys/class/gpio/gpio19/direction
echo out >/sys/class/gpio/gpio2/direction
echo out >/sys/class/gpio/gpio3/direction
echo out >/sys/class/gpio/gpio14/direction
echo out >/sys/class/gpio/gpio5/direction

echo 1 > /sys/class/gpio/gpio15/value
echo 1 > /sys/class/gpio/gpio16/value ; ls -lR /rom >/dev/null ; echo 0 > /sys/class/gpio/gpio16/value
echo 0x2c7c 0x0191 >/sys/module/option/drivers/usb-serial:option1/new_id

echo 1 > /sys/class/gpio/modem-dcdc-on/value
echo 1 > /sys/class/gpio/modem-pwrkey/value ; usleep 500000 ; echo 0 > /sys/class/gpio/modem-pwrkey/value

uci set network.lan.ipaddr='192.168.91.102'
uci commit
ifup lan

