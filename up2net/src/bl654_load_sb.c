// Load precompiled smartBASIC program into BL654 on up2net IoT-Gateway
//
// E.Lins, chip45 GmbH & Co. KG, https://www.chip45.com
//
// Pipe any uwc file into bl654_load_sb and it will be stored as $autorun$ in BL654


#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}


int check_answer(int fd)
{
	char answer[16];
	char c;
	int rdlen, i;

	i = 0;

	do {
		rdlen = read(fd, &c, 1);
		if (rdlen > 0) {
			answer[i++] = c;
			//printf("%c", (unsigned char)c);
		} else if (rdlen < 0) {
			printf("Error reading BL654 answer: %d: %s\n", rdlen, strerror(errno));
			break;
		} else {  /* rdlen == 0 */
			printf("Timeout from reading BL654 answer\n");
			break;
		}
	} while (c != '\r');
	//printf("\n");

	if (answer[0] == '\n')
		if (answer[1] == '0')
			if (answer[2] == '0')
				if (answer[3] == '\r')
					return 1;
	return 0;
}


int main(int argc, char *argv[] )
{
	char *portname = "/dev/ttyS1";
	int fd;
	int wlen, rdlen;
	int i;
	char filename[20], buf_write[40], *p;
	char c;

	char cmd_del[40];
	char cmd_fow[40];
	char cmd_fcl[40];


	if (argc > 2) {
		printf("Too many arguments.\n");
		return 0;
	} else if (argc == 2) {
		strcpy(filename, argv[1]);
	} else {
		strcpy(filename, "$autorun$");
	}

	sprintf(cmd_del, "AT+DEL \"%s\" +\r", filename);
	sprintf(cmd_fow, "AT+FOW \"%s\"\r", filename);
	sprintf(cmd_fcl, "AT+FCL\r");

	printf("Uploading UWC file as \"%s\"...\n", filename);


	fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0) {
        	printf("Error opening %s: %s\n", portname, strerror(errno));
        	return -1;
	}
	/*baudrate 115200, 8 bits, no parity, 1 stop bit */
	set_interface_attribs(fd, B115200);
	//set_mincount(fd, 0);                /* set to pure timed read */


	wlen = write(fd, cmd_del, strlen(cmd_del));
	if (wlen == 0) {
		printf("Error writing AT+DEL\n");
	}
	if (check_answer(fd) == 0) {
		printf ("Wrong answer from BL654\n");
		return 0;
	}

	wlen = write(fd, cmd_fow, strlen(cmd_fow));
	if (wlen == 0) {
		printf("Error writing AT+FOW\n");
	}
	if (check_answer(fd) == 0) {
		printf ("Wrong answer from BL654\n");
		return 0;
	}

	do {
		p = buf_write;
		for (i = 0; i < 16; ++i) {
			rdlen = read(STDIN_FILENO, &c, 1);
			if (rdlen > 0) {
				sprintf(p, "%02X", (unsigned char)c);
				p += 2;
			} else {
				break;
			}
		}
		if (i == 0)
			break;
		buf_write[2*i] = '\0';
		//printf("serout:%s\n", buf_write);
		wlen = write (fd, "AT+FWRH \"", 9);
		wlen = write (fd, buf_write, 2 * i);
		wlen = write (fd, "\"\r", 2);
		if (check_answer(fd) == 0) {
			printf ("Wrong answer from BL654\n");
			return 0;
		}
		if(i < 16)
			break;
	} while (1);

	wlen = write(fd, cmd_fcl, strlen(cmd_fcl));
	if (wlen == 0) {
		printf("Error writing AT+FCL\n");
	}
	if (check_answer(fd) == 0) {
		printf ("Wrong answer from BL654\n");
		return 0;
	}

	tcdrain(fd);    /* delay for output */

	return 1;
}

