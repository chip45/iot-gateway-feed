#!/bin/bash
# nach ein paar s aufgeben
TIMEOUT=20
( sleep $TIMEOUT; killall tr ) &
stty -F /dev/ttyS1 115200 raw -iexten -echo

echo "advscan" >/dev/ttyS1; sleep 1 ; echo "scan start" >/dev/ttyS1
tr '\r' '\n' < /dev/ttyS1 | \
{
while read cmd; do 
  if ! [ "$cmd" ]; then 
	continue; 
  fi
  cmd=${cmd/;/\",\"}
  cmd=${cmd/;/\",}
  cmd=${cmd/;/,}
  echo \{\"`date "+%d.%m.%Y %H:%M:%S"`\",`date +%s`,"\"${cmd}"\}, >>/mnt/mmcblk0p1/advscan.txt
done;

echo OK `date "+%d.%m.%Y %H:%M:%S"`
cat /mnt/mmcblk0p1/advscan.txt | ssh -y -p 50318 -i /etc/dropbear/dropbear_rsa_host_key joerg@m2m-manager.dyndns.org  "cat >>/var/www/html/advscan.txt"
if [ $? -eq 0 ] ; then
	rm /mnt/mmcblk0p1/advscan.txt
fi
}
