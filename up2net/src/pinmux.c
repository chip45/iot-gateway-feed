#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#define MMAP_PATH			"/dev/mem"


static struct pinmux {
	char *name;
	char *func[16];
	unsigned int shift;
	unsigned int mask;
} mt7688_mux[] = {
	{
        .name = "gpio",
        .func = { "gpio(0)", "gpio(1)", "refclk", "PCIeRst" },
        .shift = 0,
        .mask = 0x3,
    }, {
        .name = "spis",
        .func = { "spis", "gpio", "util", "pwm0-pwm1-uart2"},
        .shift = 2,
        .mask = 0x3,
    }, {
        .name = "spi_cs1",
        .func = { "spi_cs1", "gpio", "refclk" ,"-"},
        .shift = 4,
        .mask = 0x3,
    }, {
        .name = "i2s",
        .func = { "i2s", "gpio", "pcm", NULL },
        .shift = 6,
        .mask = 0x3,
    }, {
        .name = "uart0",
        .func = { "uart", "gpio", NULL, NULL },
        .shift = 8,
        .mask = 0x3,
    }, {
        .name = "sdmode",
        .func = { "jtag", "utif", "gpio", "sdxc" },
        .shift = 10,
        .mask = 0x3,
    }, {
        .name = "spi",
        .func = { "spi", "gpio", "-", "-" },
        .shift = 12,
        .mask = 0x1,
    }, {
        .name = "wdt",
        .func = { "wdt", "gpio", "-", "-" },
        .shift = 14,
        .mask = 0x1,
    }, {
        .name = "esd",
        .func = { "ephy", "i2s/i2c/gpio0/uart1", NULL, NULL },
        .shift = 15,
        .mask = 0x1,
    }, {
        .name = "perst",
        .func = { "perst", "gpio", NULL, NULL },
        .shift = 16,
        .mask = 0x1,
    }, {
        .name = "refclk",
        .func = { "refclk", "gpio", NULL, NULL },
        .shift = 18,
        .mask = 0x1,
    }, {
        .name = "i2c",
		.func = { "i2c", "gpio", NULL, NULL },
		.shift = 20,
		.mask = 0x3,
	}, {
		.name = "uart1",
		.func = { "uart", "gpio", NULL, NULL },
		.shift = 24,
		.mask = 0x3,
	}, {
		.name = "uart2",
		.func = { "uart", "gpio", "pwm", NULL },
		.shift = 26,
		.mask = 0x3,
	}, {
		.name = "pwm0",
		.func = { "pwm", "gpio", NULL, NULL },
		.shift = 28,
		.mask = 0x3,
	}, {
		.name = "pwm1",
		.func = { "pwm", "gpio", NULL, NULL },
		.shift = 30,
		.mask = 0x3,
    }, {
        .name = "wled_an",
        .func = { "wled", "gpio", NULL, NULL },
        .shift = 32+0,
        .mask = 0x3,
    }, {
        .name = "p0led_an",
        .func = { "p0led_an", "gpio", "rsvd", "jtag" },
        .shift = 32+2,
        .mask = 0x3,
    }, {
        .name = "p1led_an",
        .func = { "p1led_an", "gpio", "util", "jtag" },
        .shift = 32+4,
        .mask = 0x3,
    }, {
        .name = "p2led_an",
        .func = { "p2led_an", "gpio", "util", "jtag" },
        .shift = 32+6,
        .mask = 0x3,
    }, {
        .name = "p3led_an",
        .func = { "p3led_an", "gpio", "util", "jtag" },
        .shift = 32+8,
        .mask = 0x3,
    }, {
        .name = "p4led_an",
        .func = { "p4led_an", "gpio", "util", "jtag" },
        .shift = 32+10,
        .mask = 0x3,
    }, 
    // 1000003C  AGPIO_CFG  Analog GPIO Configuration 
    {
        .name = "i2s_sdi",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 0,
        .mask = 0x1,
    }, {
        .name = "i2s_sdo",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 1,
        .mask = 0x1,
    }, {
        .name = "i2s_ws",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 2,
        .mask = 0x1,
    }, {
        .name = "i2s_clk",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 3,
        .mask = 0x1,
    }, {
        .name = "ref_clko",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 4,
        .mask = 0x1,
    }, {
        .name = "wled_opendrain",
        .func = { "disable", "opendrain", NULL, NULL },
        .shift = 64 + 8,
        .mask = 0x1,
    }, {
        .name = "ephy_p0",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 16,
        .mask = 0x1,
    }, {
        .name = "ephy_p1",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 17,
        .mask = 0x01,
    }, {
        .name = "ephy_p2",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 18,
        .mask = 0x01,
    }, {
        .name = "ephy_p3",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 19,
        .mask = 0x01,
    }, {
        .name = "ephy_p4",
        .func = { "analog", "digital", NULL, NULL },
        .shift = 64 + 20,
        .mask = 0x01,
    }, {
        .name = "ephy_p1x4",
        .func = { "analog", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, "digital" },
        .shift = 64 + 17,
        .mask = 0x0f,
    }
};
int __MUX_MAX = sizeof(mt7688_mux)/sizeof(mt7688_mux[0]); 


static uint8_t* gpio_mmap_reg = NULL;
static int gpio_mmap_fd = 0;

static void __gpiomode_set(unsigned int mask, unsigned int shift, unsigned int val)
{
	volatile uint32_t *reg = (volatile uint32_t*) (gpio_mmap_reg + 0x60);
	unsigned int v;

    if (shift >= 64)
    {
        shift -= 64;
        reg = (volatile uint32_t*)(gpio_mmap_reg + 0x3c);
    }
    else if (shift >= 32)
    {
		shift -= 32;
		reg++; // 0x64
	}

	v = *reg;
	v &= ~(mask << shift);
	v |= (val << shift);
	*(volatile uint32_t*) (reg) = v;
}

static int gpiomode_set(char *group, char *name)
{
	int i;
	int id;
	struct pinmux *pmux = NULL;

	for (id = 0; id < __MUX_MAX; id++)
		if (strcmp(mt7688_mux[id].name, group) == 0)
		{
			pmux = &mt7688_mux[id];
			break;
		}

	if (pmux)
	{
		for (i = 0; i < 16; i++)
		{
			if (!pmux->func[i] || strcmp(pmux->func[i], name))
				continue;
			__gpiomode_set(pmux->mask, pmux->shift, i);
			fprintf(stderr, "set pinmux %s -> %s\n", pmux->name, name);
			return 0;
		}
	}
	fprintf(stderr, "unknown group/function combination\n");
	return -1;
}

static int gpio_get(void)
{
    unsigned msk;
    int i;

    volatile uint32_t* gpioDirReg = (volatile uint32_t*)(gpio_mmap_reg + 0x600);
    volatile uint32_t* gpioPolReg = (volatile uint32_t*)(gpio_mmap_reg + 0x610);
    volatile uint32_t* gpioDataReg = (volatile uint32_t*)(gpio_mmap_reg + 0x620);
    //volatile uint32_t* gpioSetReg = (volatile uint32_t*)(gpio_mmap_reg + 0x630);
    //volatile uint32_t* gpioResReg = (volatile uint32_t*)(gpio_mmap_reg + 0x640);
    unsigned int gpioDir = *gpioDirReg;
    unsigned int gpioPol = *gpioPolReg;
    unsigned int gpioData = *gpioDataReg;
    //printf("GREG dir %08x pol %08x data %08x\n", (unsigned)gpioDirReg, (unsigned)gpioPolReg, (unsigned)gpioDataReg);
    printf("GPIO dir %08x pol %08x data %08x\n", gpioDir, gpioPol, gpioData);
    for (i = 0, msk = 1; i < 32; msk <<= 1, i++)
    {
        printf("GPIO%d %s %d pol %d\n", i, (gpioDir&msk) ? "out" : "in ", (gpioData & msk) ? 1 : 0, (gpioPol & msk) ? 1 : 0);
    }
    return 0;
}

static int gpio_set(const char *sName, const char *sVal)
{
    int iGpio;
    unsigned uGpioMsk;
    if (strncmp(sName, "GPIO", 4) != 0 && strncmp(sName, "gpio", 4) != 0)
    {
        printf("Invalid name: %s %s\n", sName, sVal);
        return -1;
    }
    iGpio = atoi(sName + 4);
    if (iGpio < 0 || iGpio >= 32)
    {
        printf("Invalid Number: gpio%d %s\n", iGpio, sVal);
        return -1;
    }
    uGpioMsk = 1 << iGpio;
    volatile uint32_t* gioDirReg = (volatile uint32_t*)(gpio_mmap_reg + 0x600);
    volatile uint32_t* gioSetReg = (volatile uint32_t*)(gpio_mmap_reg + 0x630);
    volatile uint32_t* gioResReg = (volatile uint32_t*)(gpio_mmap_reg + 0x640);
    if (strcmp(sVal, "out") == 0)
    {
        *gioDirReg = *gioDirReg | uGpioMsk;
        printf("set gpio%d out 0x%08x %08x %08x\n", iGpio, (unsigned int)gioDirReg, uGpioMsk, *gioDirReg);
    }
    else if (strcmp(sVal, "in") == 0)
    {
        *gioDirReg &= ~uGpioMsk;
        printf("set gpio%d in 0x%08x %08x\n", iGpio, (unsigned int)gioDirReg, uGpioMsk);
    }
    else if (strcmp(sVal, "1") == 0)
    {
        *gioSetReg = uGpioMsk;
        printf("set gpio%d 1 0x%08x %08x\n", iGpio, (unsigned int)gioSetReg, uGpioMsk);
    }
    else if (strcmp(sVal, "0") == 0)
    {
        *gioResReg = uGpioMsk;
        printf("set gpio%d 0 0x%08x %08x\n", iGpio, (unsigned int)gioResReg, uGpioMsk);
    }
    else
    {
        printf("Invalid Arg: %s %s\n", sName, sVal);
        return -1;
    }
    return 0;
}

static int gpiomode_get(void)
{
	unsigned int reg = *(volatile uint32_t*) (gpio_mmap_reg + 0x60);
    unsigned int reg2 = *(volatile uint32_t*)(gpio_mmap_reg + 0x64);
    unsigned int agpio = *(volatile uint32_t*)(gpio_mmap_reg + 0x3c);
    int id;
	struct pinmux *pmux;

	for (id = 0, pmux = mt7688_mux; id < __MUX_MAX; id++, pmux++)
    {
		unsigned int val;
        unsigned int shift;
        const char *sreg;
		int i;

        shift = pmux->shift;
        if (pmux->shift < 32)
        {
            val = (reg >> shift) & pmux->mask;
            sreg = "gpio1_mode";
        }
        else if (pmux->shift < 64)
        {
            shift -= 32;
            val = (reg2 >> shift) & pmux->mask;
            sreg = "gpio2_mode";
        }
        else
        {
            shift -= 64;
            val = (agpio >> shift) & pmux->mask;
            sreg = "agpio__cfg";
        }

        fprintf(stderr, "Group_%s_%02d_%02x %-8s - ", sreg, shift, val, pmux->name);
		for (i = 0; i < 16; i++) 
        {
            if (!pmux->func[i])
                continue;
			if (i == val)
				fprintf(stderr, "[%s] ", pmux->func[i]);
			else
				fprintf(stderr, "%s ", pmux->func[i]);
		}
		fprintf(stderr, "\n");
	}

	return 0;
}

static int gpiomode_mmap(void)
{
	if ((gpio_mmap_fd = open(MMAP_PATH, O_RDWR)) < 0) {
		fprintf(stderr, "unable to open mmap file");
		return -1;
	}

	gpio_mmap_reg = (uint8_t*) mmap(NULL, 0x800, PROT_READ | PROT_WRITE, MAP_FILE | MAP_SHARED, gpio_mmap_fd, 0x10000000);
	if (gpio_mmap_reg == MAP_FAILED) {
		perror("foo");
		fprintf(stderr, "failed to mmap");
		gpio_mmap_reg = NULL;
		close(gpio_mmap_fd);
		return -1;
	}
    printf("gpiomap %08x\n",(unsigned) gpio_mmap_reg);
	return 0;
}

int main(int argc, char **argv)
{
	int ret = -1;

	if (gpiomode_mmap())
		return -1;

    if (argc >= 4)
    {
        if (!strcmp(argv[1], "set"))
        {
            ret = gpiomode_set(argv[2], argv[3]);
        }
        else if (!strcmp(argv[1], "setgpio"))
        {
            ret = gpio_set(argv[2], argv[3]);
        }
    }
    else if (argc >= 2)
    {
        if (!strcmp(argv[1], "get"))
        {
            ret = gpiomode_get();
        }
        else if (!strcmp(argv[1], "getgpio"))
        {
            ret = gpio_get();
        }
    }
    else {
		fprintf(stderr, 
            "%s set <group> <function>\n"
            "%s get\n"
            "%s getgpio\n"
            "%s setgpio gpio<num> [in,out,0,1]\n", *argv, *argv, *argv, *argv);
    }

	close(gpio_mmap_fd);

	return ret;
}
