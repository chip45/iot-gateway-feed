# BL654 smartBASIC upload
The up2net feed includes a tool for uploading precompiled smartBASIC programs (.uwc) to the Laird BL654 Bluetooth LE module on the up2net IoT-Gateway. A default BLE advert scanner application is included as well.
## Usage
The bl654_load_sb tool and the up2net-advscan.uwc smartBASIC program are located in folder /up2net. The uwc file has to be piped into the tool and a target filename can be supplied as command line parameter. If the parameter is omitted, it uses $autostart$ as default.

Make sure, that no program is running on the BL654 before starting the upload. If unsure, use microcom to check, if the BL654 is in command mode:
```
microcom -s 115200 /dev/ttyS1
```
Press enter to see, if it answers with "00".

Now you can upload the uwc program:
```
cd up2net
cat up2net-advscan.uwc | ./bl654_load_sb advscan
```
The tool will then upload the smartBASIC program as advscan to the target. If you ommit the name, it uploads as $autorun$.

Autorun programs always start on power up automatically. If using this, make sure to be able to exit your program to return to command mode to be able to upload further programs. Read Laird's smartBASIC documentation.

## up2net BLE Scanner
The up2net-advscan smartBASIC program is available here: https://github.com/up2net/ble_scanner. Read the documentation for functionality and available commands.

A precompiled uwc file is included with the up2net IoT-Gateway through this feed here.
