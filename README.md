# up2net IoT-Gateway OpenWRT Feed
This is an OpenWRT feed for the chip45 up2net IoT-Gateway (http://www.chip45.com/categories/iot_edge_gateways_bluetooth_ble_wifi_lte_cellular_linux_openwrt.php). Adding this feed to OpenWRT will include all packages, drivers etc. necessary for the IoT-Gateway.
## Install Linux Tools
Building OpenWRT requires some extra linux tools, which might not be present in the regular Ubuntu installation. The necessary tools can be installed with apt-get.
```
sudo apt-get install git g++ make libncurses5-dev subversion libssl-dev gawk libxml-parser-perl unzip wget python xz-utils
```
## Download and Install OpenWRT
OpenWRT and this feed can be installed under Linux with git. We currently use the branch V18.06.2. The following commands will do the job.
```
git clone https://git.openwrt.org/openwrt/openwrt.git --branch=v18.06.2
git clone https://github.com/up2net/iot-gateway-feed.git
cd openwrt
./scripts/feeds update
./scripts/feeds install -a
patch -p1 < ../iot-gateway-feed/stuff/new-up2net-board.patch
cp ../iot-gateway-feed/kernelpatches/*.patch target/linux/ramips/patches-4.14/
mkdir -p package/feeds/iot-gateway-feed
ln -s `realpath ../iot-gateway-feed/up2net/` package/feeds/iot-gateway-feed/
```
## Configuring OpenWRT
Configuring OpenWRT is done with
```
make menuconfig
```
Do the following target selections on the main page:
```
Target System (MediaTek Ralink MIPS)
Subtarget (MT76x8 based boards)
Target Profile (Chip45 up2net IoT Gateway)
```
Then add the additional packages on the UP2NET page:
UP2NET --->
```
<*> busyboxmeta1.......... Metapackage for busybox: microcom, usleep and lusb
<*> metafilesys.............................. Metapackage SD-card, ext4, vfat
<*> metakernelmod............................. Metapackage for kernel modules
<*> metaop2nssl...................................... Metapackage for openssl
<*> up2net-tools................................. UP2NET Tools io and ser2net
```
Exit make menuconfig and save configuration.
## Building OpenWRT
Now we can start the build process with:
```
make
ls -l bin/targets/ramips/mt76x8/
```
The last ls command should show the new image openwrt-ramips-mt76x8-up2net-squashfs-sysupgrade.bin in that folder.
## Installing on the IoT-Gateway
If the image was built successfully you can copy the image to the target device with scp command. Use the correct IP address of the device.
```
scp bin/targets/ramips/mt76x8/openwrt-ramips-mt76x8-up2net-squashfs-sysupgrade.bin root@192.168.1.1:/tmp/
```
Then log in to the console of the device (either with a terminal through USB virtual COM port or with ssh) and start the upgrade process.
```
sysupgrade -v /tmp/openwrt-ramips-mt76x8-up2net-squashfs-sysupgrade.bin
```
The upgrade process will take a while and the WiFi LED of the IoT-Gateway will flash during this. Do not interrupt the upgrade process! The gateway will reboot automatically. This first reboot takes some more time than usually, because the flash disk will be erased.

Alternatively you can upgrade the IoT-Gateway firmware through the Luci web configuration interface.
